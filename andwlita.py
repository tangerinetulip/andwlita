import time

import cherrypy
import face_recognition
import falcon
import falcon_jsonify
import numpy
import peewee
import requests

# region peewee
db = peewee.SqliteDatabase(None)


class BaseModel(peewee.Model):
    class Meta:
        database = db


class Person(BaseModel):
    name = peewee.CharField(unique=True)


class Face(BaseModel):
    person = peewee.ForeignKeyField(Person)
    face = peewee.BlobField()
# endregion


# region Falcon
def before_request(req, resp, resource, params):
    if req.content_length == 0 and req.method == 'POST':
        raise falcon.HTTPBadRequest('Empty request',
                                    'Please fill required fields.')
    db.init('andwlita.db')
    db.create_tables([Person, Face], safe=True)


def after_request(req, resp, resource):
    db.close()


@falcon.before(before_request)
@falcon.after(after_request)
class FaceStorage(object):
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        people = Person.select()
        body = '{"Names":"'
        start = time.time()
        for person in people:
            body += person.name + ','
        if body == '':
            raise falcon.HTTPBadRequest('Empty database',
                                        'Please register some people first.')
        else:
            body += '"}'
            time_taken = str(time.time() - start)
            body += ', "Time taken":"' + time_taken + '"}'
            resp.body = body

    def on_post(self, req, resp):
        process = req.get_json('process')
        person_name = req.get_json('name')
        image_url = req.get_json('image')
        image = requests.get(image_url, stream=True).raw
        new_face = face_recognition.load_image_file(image)
        new_face_encoding = face_recognition.face_encodings(new_face)[0]
        face_numpy = numpy.ndarray.dumps(new_face_encoding)
        start = time.time()
        if process == 'new':
            try:
                with db.transaction():
                    Person.create(name=person_name)
                    person = Person.get(Person.name == person_name)
                    Face.create(person=person,
                                face=face_numpy)
                time_taken = str(time.time() - start)
                resp.body = '{"Name":"' + person_name + '", "Registration":"Success", "Time Taken":"' + time_taken + '"}'
            except peewee.IntegrityError:
                raise falcon.HTTPBadRequest('Name taken',
                                            'Please choose another name.')
        elif process == 'add':
            person = Person.get(Person.name == person_name)
            Face.create(person=person,
                        face=face_numpy)
            time_taken = str(time.time() - start)
            resp.body = '{"Name":"' + person_name + '", "Addition":"Success", "Time Taken":"' + time_taken + '"}'


@falcon.before(before_request)
@falcon.after(after_request)
class FaceRecognition(object):
    def on_post(self, req, resp):
        try:
            image_url = req.get_json('image')
            image = requests.get(image_url, stream=True).raw
            unknown_face = face_recognition.load_image_file(image)
            unknown_face_encoding = face_recognition.face_encodings(unknown_face)[0]
            known_faces = Face.select()
            start = time.time()
            for face in known_faces:
                known_face = numpy.loads(face.face)
                results = face_recognition.compare_faces([known_face], unknown_face_encoding, tolerance=0.44)
                time_taken = str(time.time() - start)
                if results[0]:
                    result = '{"Person":"' + face.person.name + '", "Time Taken":"' + time_taken + '"}'
                    resp.status = falcon.HTTP_202
                    resp.body = result
                    break
                elif not results[0]:
                    result = '{"Person":"Unknown", "Time Taken":"' + time_taken + '"}'
                    resp.status = falcon.HTTP_202
                    resp.body = result
        except KeyError:
            raise falcon.HTTPBadRequest('Missing image',
                                        'You must provide an image.')
        except requests.exceptions.MissingSchema:
            raise falcon.HTTPBadRequest('Invalid URL',
                                        'Please provide a proper URL.')
# endregion

app = falcon.API(middleware=[falcon_jsonify.Middleware(help_messages=True)])
recognize = FaceRecognition()
storage = FaceStorage()
app.add_route('/face/recognize', recognize)
app.add_route('/face/register', storage)
cherrypy.tree.graft(app, "/")
cherrypy.engine.start()
cherrypy.engine.block()
